/***Classe per modello post da http://jsonplaceholder.typicode.com/posts */

export class Post {

    userId:string;
    id:number;
    title:string;
    body:string;
}
