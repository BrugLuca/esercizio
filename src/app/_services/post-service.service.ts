import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../_models/post';
import { Comment } from '../_models/comment';


@Injectable({
  providedIn: 'root'
})
export class PostServiceService {

  url = 'http://jsonplaceholder.typicode.com/';
  //arrivo :string;

  constructor(private http: HttpClient) { }

  public buildMeAnUrl(testo: string) {
    testo = this.url + testo;
    return this.http.get<Post[]>(testo); //qua posso quindi definire un tipo così
  }

  public commentsGET(testo: string) {
    testo = this.url + testo;
    return this.http.get<Comment[]>(testo); //qua posso quindi definire un tipo così
  }


}
