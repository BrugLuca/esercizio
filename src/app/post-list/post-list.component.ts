import { Component, OnInit } from '@angular/core';
import { PostServiceService } from '../_services/post-service.service';
import { Post } from '../_models/post';
import { filter, switchMap, concatMap } from 'rxjs/operators';

import { from, of } from 'rxjs';
import { Comment } from '../_models/comment';
import { state, trigger, style, transition, animate } from '@angular/animations';

 /* trigger('uscita', [
         transition(':leave', [
           animate('5s', style({ height: '0px' }))
         ]),
       ])*/


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
  animations: [
    trigger('entrata', [

      transition(':enter', [
        style({ height: '0px' }),
        animate('0.5s', style({ height: '50px' }))
      ]),
      transition(':leave', [
        animate('5s', style({ height: '0px' }))
      ])
     
    
      ])
      ]
    }
    )


export class PostListComponent implements OnInit {

  ok: boolean;
  corrente: string;
  posts: Post[] = [];
  comments: Comment[] = [];

  constructor(private postService: PostServiceService) {
    //this.posts=new Post[];
    this.ok = false;
  }

  ngOnInit() {

  }

  chiamaCommenti() {
    while (!this.ok) {
      this.postService.buildMeAnUrl('posts').pipe(

        switchMap(ingresso => from(ingresso)),
        //da array a singolo post
        filter(notfiltered => (notfiltered.id % 2 == 0)),
        //da tutti a solo quelli con id pari
        concatMap(voci => this.postService.commentsGET('comments?postId=' + voci.id)),
        //concateno i segnali di tutti i commenti tra loro
        switchMap(ingresso => of(ingresso[ingresso.length - 1]))
        //qui mi sente solo l'ultimo post, invece di last mi conviene un of (=> un nuovo obs) del ultimo elemento array

      ).subscribe(
        voci => {
          this.comments.push(voci);
          /*  
            this.postService.commentsGET('comments?postId='+voci.id).pipe(switchMap(ingresso => from(ingresso)), last()).subscribe(
              voci => {
                this.comments.push(voci)     // voci => from(voci).subscribe(test => test filter post => post%2==0 )
                console.log(voci.postId);
                console.log(voci.id);
                console.log(voci.body);
              }
            );*/

          // voci => from(voci).subscribe(test => test filter post => post%2==0 )
          //console.log(voci.id);
        }
      );
      this.ok = true;
    }

  }

  rimuoviCommenti() {

    this.ok = false;
  }

}
